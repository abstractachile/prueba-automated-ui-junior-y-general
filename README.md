# Ejercicio práctico: UI Automation 

(Junior / Junior Advance o General)

Crear ejercicio de automatización UI con Selenium o WebDriverIO que realice los siguientes pasos:

```
1. Abrir web de http://opencart.abstracta.us/ (Puede utilizar cualquier navegador).
2. En la barra de búsqueda, ingresar el producto “iPhone” y buscar.
3. Seleccionar la primera opción que aparezca.
4. Agregar el producto al carrito de compras.
5. Ingresar al botón del carrito de compras.
6. Presionar "View Cart".
7. Validar que el iPhone seleccionado se encuentre en el carrito de compras.
8. Remover el iPhone del carrito de compras.
9. Validar que el iPhone ya no se encuentre en el carrito de compras.
10. Realizar capturas de pantalla en los pasos del test que se consideren necesarios.
```

Se valorará:
```
- Aplicación del patrón de diseño “Page Object”.
- Creación de un framework de automatización. Ejemplo: Selenium: Maven, TestNG, Cucumber, etc. WebDriverIO: Mocha, Chai, Jasmine, Cucumber, etc.
- Buenas prácticas de programación (utilización de selectores, assertions, tiempos de espera, etc.)
```
Durante la demo de revisión con el líder técnico se solicitará:

```
Ejecutar la automatización con la búsqueda del producto.
Explicar la elección de selectores, assertions, librerías instaladas y patrón de diseño utilizado, entre otros.
```

El entregable debe ser un repositorio en GitLab o GitHub (público), conteniendo la automatización que resuelve el problema. 

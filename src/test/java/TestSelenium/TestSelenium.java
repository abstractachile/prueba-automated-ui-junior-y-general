package TestSelenium;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import Pages.CartPageOpenCart;
import Pages.HomePageOpenCart;


public class TestSelenium {
	
	private static WebDriver driver;
	private String baseUrl;

	
	@BeforeMethod
	public void setUp() throws Exception {
		
		// initializing driver variable using Chromedriver
		driver = new ChromeDriver();
		
		// maximized the browser window
		driver.manage().window().maximize();
		
		// launching opencart on the browser
		baseUrl = "http://opencart.abstracta.us/";		
	}
	
	
	@Test
	public void SendProduct() throws InterruptedException, IOException {	
		
		//Pages import
		HomePageOpenCart homeopencart = new HomePageOpenCart(driver);
		CartPageOpenCart cartopencart = new CartPageOpenCart(driver);
		
		//1. Abrir web de http://opencart.abstracta.us/ (Puede utilizar cualquier navegador).
		driver.get(baseUrl);
				
		//2. En la barra de b�squeda, ingresar el producto �iPhone� y buscar.
		homeopencart.searchProduct("iPhone");
		
		//3. Seleccionar la primera opci�n que aparezca.
		homeopencart.selectProduct();
		
		//4. Agregar el producto al carrito de compras.
		homeopencart.addToCart();
		
		//5. Ingresar al bot�n del carrito de compras.
		homeopencart.buttonCart();
		//driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(3));
		
		Thread.sleep(2000);
		//6. Presionar "View Cart".
		homeopencart.ViewCart();
		
		//7. Validar que el iPhone seleccionado se encuentre en el carrito de compras.
		cartopencart.validateProductCart();		
		//Take screenshot
        File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
        FileUtils.copyFile(scrFile, new File("TestSendProduct/ValidateProductCart01.png"));
		
		//8. Remover el iPhone del carrito de compras.
		cartopencart.removeProductCart();
		
		Thread.sleep(2000);
		//9. Validar que el iPhone ya no se encuentre en el carrito de compras.
		cartopencart.cartEmpty();
		//Take screenshot
        File scrFile2 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
        FileUtils.copyFile(scrFile2, new File("TestSendProduct/ValidateCartEmptyt02.png"));
		
	}

	@AfterMethod
	public void tearDown() throws Exception {
		// closes all the browser windows opened by web driver
		driver.quit();
	}
	



}

package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;

public class CartPageOpenCart {
	
	/*
	 * Locators
	 */
	By product_cart = By.xpath("//*[@id=\"content\"]/form/div/table/tbody/tr/td[2]/a");
	By button_remove = By.xpath("//*[@id=\"content\"]/form/div/table/tbody/tr/td[4]/div/span/button[2]");
	By title_empty = By.xpath("//*[@id=\"content\"]/p");
	
	/*
	 * Drivers
	 */
	WebDriver driver;
	public CartPageOpenCart(WebDriver driver) {
		this.driver = driver;
	}
	
	public void validateProductCart() {
		Assert.assertEquals("iPhone", driver.findElement(product_cart).getText()); 
	}
	
	public void removeProductCart() {
		driver.findElement(button_remove).click();
	}
	
	public void cartEmpty() {
		Assert.assertEquals("Your shopping cart is empty!", driver.findElement(title_empty).getText()); 
	}

}
